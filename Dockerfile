FROM php:8-apache

# Add composer from Docker image (multi-stage build)
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Install required extensions
RUN apt update && apt install -y libpq-dev libxml2-dev openssl
RUN docker-php-ext-install bcmath ctype fileinfo pdo pdo_pgsql xml

# Change Apache document root
RUN sed -ri -e 's!/var/www/html!/var/www/html/public!g' /etc/apache2/sites-available/*.conf

# Copy project files
WORKDIR /var/www/
COPY . .

# Setup permissions
# RUN chown -R www-data:www-data storage