# Docker Compose
## Symfony
### Requirements
- MariaDB 10: an available MariaDB server running on port `3306` and host `db` with the username as `symfony`, the password as `symfony` and a database named `symfony`.

### How to run
composer install

add docker-compose.yml & Dockerfile

docker compose up -d